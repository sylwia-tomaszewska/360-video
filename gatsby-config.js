module.exports = {
    siteMetadata: {
        siteUrl: `https://localhost:8000`,
    },
    plugins: [
        `gatsby-plugin-sass`,
        `gatsby-plugin-image`,
        `gatsby-plugin-sharp`,
        `gatsby-transformer-sharp`,
        `gatsby-plugin-styled-components`,
        `gatsby-plugin-root-import`,
        `gatsby-plugin-react-svg`,
        {
            resolve: `gatsby-plugin-google-fonts`,
            options: {
                fonts: [`Work Sans\:400,700`],
                display: 'swap',
            },
        },
        {
            resolve: 'gatsby-plugin-manifest',
            options: {
                icon: 'src/static/favicon-32x32.png',
            },
        },
    ],
};