import * as React from 'react';
import { GlobalStyle } from 'styles/Global.styles';
import {
    Navbar,
    SectionHero,
    Wrapper,
    Bttn,
    HeroImage,
    SectionAbout,
    AboutTxt,
    SectionBenefits,
    BenefitsCol,
    BenefitBox,
    SectionContact,
    SectionGallery,
    GalleryImg,
    Footer,
} from 'styles/Component.styles';
import Logo from 'images/vr-logo.svg';
import Arrow from 'images/arrow.svg';
import Immersion from 'images/immersive.svg';
import Broad from 'images/width.svg';
import Innovation from 'images/innovation.svg';
import Gallery1 from 'images/video360-1.jpg';
import Gallery2 from 'images/video360-2.jpg';
import Gallery3 from 'images/video360-3.jpg';
import Gallery4 from 'images/video360-4.jpg';
import Open from 'images/open.svg';

const IndexPage = () => (
    <>
        <GlobalStyle />
        <title> Hello World - video 360 </title>
        <SectionHero>
            <Wrapper>
                <Navbar>
                    <Logo />
                    <Bttn as="a" href="mailto:s.k.tomaszewska@gmail.com?subject=Nice%20website%20-%20I%20want%20one%20too%20%3A)" target="_blank" rel="noopener noreferrer">
                        Contact us
                    </Bttn>
                </Navbar>
                <h1>Look around you and see what is hidden</h1>
                <p>thanks to video 360</p>
                <HeroImage />
            </Wrapper>
        </SectionHero>
        <SectionAbout>
            <Wrapper>
                <AboutTxt>
                    <Arrow className="arrow--top" />
                    <h2>What is video 360?</h2>
                    <p>
                        360-degree videos, also known as surround video, or immersive videos or spherical videos, are video recordings where a view in every direction is recorded at the same time,
                        shot using an omnidirectional camera or a collection of cameras. During playback on normal flat display the viewer has control of the viewing direction like a panorama. It can
                        also be played on a display or projectors arranged in a sphere or some part of a sphere. [Source: Wikipedia]
                    </p>
                    <Arrow className="arrow--bottom" />
                </AboutTxt>
            </Wrapper>
        </SectionAbout>
        <SectionBenefits>
            <Wrapper>
                <h2>What can you achieve?</h2>
                <BenefitsCol>
                    <BenefitBox>
                        <Innovation />
                        <p>Innovative and attractive form of presentation, more interesting than traditional methods of showing space</p>
                    </BenefitBox>
                    <BenefitBox>
                        <Immersion />
                        <p>Interactivity and immersion that makes you feel as if you are there</p>
                    </BenefitBox>
                    <BenefitBox>
                        <Broad />
                        <p>Large space for product demonstration, as opposed to photos and standard video</p>
                    </BenefitBox>
                </BenefitsCol>
            </Wrapper>
        </SectionBenefits>
        <SectionGallery>
            <GalleryImg css={{ backgroundImage: `url(${Gallery1})` }} />
            <GalleryImg css={{ backgroundImage: `url(${Gallery2})` }} />
            <GalleryImg css={{ backgroundImage: `url(${Gallery3})` }} />
            <GalleryImg css={{ backgroundImage: `url(${Gallery4})` }} />
        </SectionGallery>
        <SectionContact>
            <Wrapper>
                <h2>Sound interesting?</h2>
                <p>Write to us and we will tell you more ;)</p>
                <Bttn as="a" href="mailto:s.k.tomaszewska@gmail.com?subject=Nice%20website%20-%20I%20want%20one%20too%20%3A)" target="_blank" rel="noopener noreferrer">
                    Contact us
                </Bttn>
            </Wrapper>
        </SectionContact>
        <Footer>
            <Wrapper>
                <small>
                    Check out the code in the{' '}
                    <a href="https://gitlab.com/sylwia-tomaszewska/cape-morris" target="_blank" rel="noopener noreferrer">
                        Gitlab repository <Open className="footer_icon" />
                    </a>
                </small>
            </Wrapper>
        </Footer>
    </>
);

export default IndexPage;
