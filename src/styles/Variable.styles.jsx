export const colors = {
    dark: '#191919',
    darkLighter: '#333333',
    light: '#fff',
    lightDarker: '#c8c8c8',
    prim: '#0443fc',
    primDarker: '#0234ca',
    sec: '#7c8cfb',
};

export const fonts = {
    h1: '3.713rem',
    h2: '2.856rem',
    h3: '2.197rem',
    h4: '1.69rem',
    h5: '1.3rem',
    sm: '0.8rem',
    md: '1rem',
};

export const lines = {
    sm: '1.2',
    md: '1.3',
    lg: '1.4',
};

export const queries = {
    sm: '@media screen and (min-width: 26rem)',
    md: '@media screen and (min-width: 40rem)',
    lg: '@media screen and (min-width: 63rem)',
    xl: '@media screen and (min-width: 100rem)',
};

export const sizes = {
    sm: '26rem',
    md: '40rem',
    lg: '63rem',
    xl: '100rem',
};

export const shadows = {
    sm: '0px 2px 8px 0px rgba(25, 25, 25, 0.18)',
    md: '0px 4px 12px 0px rgba(25, 25, 25, 0.16)',
};

export const transitions = {
    fast: 'all 0.2s ease-in-out',
};
