import 'styles/reset.css';
import { createGlobalStyle } from 'styled-components';
import { colors, fonts, lines } from './Variable.styles';

export const GlobalStyle = createGlobalStyle`
  *, *::before, *::after {
    box-sizing: border-box;
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
    -moz-osx-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
  }
  html{
    font-size: 18px;
    font-size: clamp(1rem, 0.9122rem + 0.3801vw, 1.3rem);
  }
  body {
    width: 100%;
    height: 100%;
    min-height: 100vh;
    overflow-x: hidden;
    margin: 0;
    color: ${colors.dark};
    font-family: 'Work Sans';
    font-size: ${fonts.md};
    line-height: ${lines.lg}
  }
  h1, h2, h3, h4, h5 {
      font-weight: bold;
      line-height: ${lines.sm};
  }
  h1 {
      font-size: ${fonts.h1};
  }
  h2 {
      font-size: ${fonts.h2};
  }
  h3 {
      font-size: ${fonts.h3};
  }
  h4 {
      font-size: ${fonts.h4};
  }
  h5 {
      font-size: ${fonts.h5};
  }
`;
