import styled from 'styled-components';
import { sizes, colors, fonts, lines, transitions } from 'styles/Variable.styles';
import hero from 'images/hero.jpg';
import { shadows } from './Variable.styles';

const Navbar = styled.nav`
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 1rem;
    svg {
        width: 100%;
        max-width: 8rem;
        height: 100%;
    }
`;
const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    max-width: ${(props) => (props.maxMd ? sizes.md : sizes.xl)};
    height: 100%;
    margin: 0 auto;
    padding-left: clamp(1rem, 5vw, 7rem);
    padding-right: clamp(1rem, 5vw, 7rem);
`;
const Bttn = styled.a`
    display: flex;
    width: 100%;
    max-width: max-content;
    padding: 0.5em 1em;
    background: ${colors.prim};
    border-radius: 10rem;
    box-shadow: ${shadows.sm};
    color: ${colors.light};
    transition: ${transitions.fast};
    cursor: pointer;
    :hover {
        background: ${colors.primDarker};
        box-shadow: none;
    }
`;
const SectionHero = styled.div`
    display: flex;
    flex-direction: column;
    min-height: 80vh;
    padding: 2rem 0 0;
    h1,
    p {
        width: 100%;
        max-width: ${sizes.md};
        margin: auto;
        text-align: center;
    }
    h1 {
        margin: 2rem auto 1rem;
        line-height: ${lines.sm};
    }
    p {
        font-size: ${fonts.h5};
        line-height: ${lines.md};
    }
`;
const HeroImage = styled.div`
    width: 100%;
    max-width: ${sizes.lg};
    height: calc(${sizes.md} * 0.6);
    margin: 2rem auto;
    margin-bottom: calc(-${sizes.sm} * 0.2);
    background: url(${hero}) no-repeat center 25%;
    background-size: cover;
    border-radius: 1rem;
    box-shadow: ${shadows.md};
`;
const SectionAbout = styled.div`
    padding: 2rem 0;
    padding-top: calc(${sizes.sm} * 0.2);
    background: ${colors.prim};
    color: ${colors.light};
    .intro_content {
    }
    h2 {
        margin-bottom: 1rem;
    }
`;
const AboutTxt = styled.div`
    position: relative;
    width: 100%;
    max-width: calc(${sizes.md} * 0.8);
    margin: 6rem auto 4rem;
    margin-left: 45%;
    .arrow {
        &--top,
        &--bottom {
            position: absolute;
            width: 6rem;
            height: 9rem;
            stroke: ${colors.dark};
            opacity: 0.6;
        }
        &--top {
            top: -6rem;
            left: -30%;
        }
        &--bottom {
            bottom: -8rem;
            left: -20%;
            transform: rotate(45deg);
        }
    }
`;
const SectionBenefits = styled.section`
    padding: 4rem 0 5rem;
    background: ${colors.prim};
    color: ${colors.light};
    h2 {
        margin-bottom: 2rem;
        text-align: center;
    }
`;
const BenefitsCol = styled.div`
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    gap: 3rem;
    width: 100%;
    max-width: ${sizes.lg};
    margin: auto;
`;
const BenefitBox = styled.div`
    display: grid;
    grid-template-rows: auto 1fr;
    gap: 2rem;
    justify-content: center;
    padding: 2rem;
    background: ${colors.prim};
    box-shadow: ${shadows.sm};
    border: 2px solid ${colors.dark};
    border-radius: 0.25em;
    text-align: center;
    svg {
        width: 5rem;
        height: 5rem;
        margin: auto;
        fill: ${colors.dark};
    }
`;
const SectionContact = styled.section`
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 6rem 0;
    text-align: center;
    p {
        margin-top: 0.5rem;
    }
    a {
        margin: auto;
        margin-top: 2rem;
        font-size: ${fonts.h5};
    }
`;
const SectionGallery = styled.section`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
`;
const GalleryImg = styled.div`
    position: relative;
    width: 100%;
    height: 16rem;
    background: ${colors.prim};
    background-size: cover;
    transition: ${transitions.fast};
    &:hover {
        background-blend-mode: multiply;
        &::after {
            position: absolute;
            content: '+';
            font-size: ${fonts.h1};
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: ${colors.light};
            line-height: 1;
        }
    }
`;
const Footer = styled.footer`
    padding: 1rem 0;
    background: ${colors.lightDarker};
    font-size: ${fonts.sm};
    a {
        font-weight: bold;
    }
    .footer_icon {
        width: 0.8em;
        height: 0.8em;
        margin-bottom: -0.1em;
        fill: ${colors.darkLighter};
    }
`;

export { Navbar, Bttn, SectionHero, Wrapper, HeroImage, SectionAbout, AboutTxt, SectionBenefits, BenefitsCol, BenefitBox, SectionContact, SectionGallery, GalleryImg, Footer };
